package stellar.service;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import stellar.dao.mem.InMemoryPlanetDAO;
import stellar.dao.mem.InMemorySystemDAO;
import stellar.model.Planet;
import stellar.service.impl.StellarServiceImpl;

import java.util.List;

public class StellarServiceStart {

    public static void main(String[] args) {
        System.out.println("Let's explore!");

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("stellar");
        StellarService service = applicationContext.getBean(StellarService.class);

        List<Planet> planets = service.getPlanets(service.getSystemById(1));

        planets.forEach(p-> System.out.println(p));

    }
}
