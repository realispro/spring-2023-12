package stellar.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@RestController
@Slf4j
@RequiredArgsConstructor
public class StellarRest {

    private final StellarService stellarService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;

    @GetMapping(value = "/systems", produces = "application/json") // /systems?phrase=Solar
    List<PlanetarySystem> getSystems(
            @RequestParam(value = "phrase", required = false) String phrase,
            @RequestHeader(value = "foo", required = false) String fooHeader,
            @RequestHeader Map<String,String> headers,
            @CookieValue(value = "someCookie", required = false) String someCookie
    ){
        log.info("about to retrieve systems");
        log.info("foo header: {}", fooHeader);
        headers.entrySet().stream()
                .filter(e->e.getKey().startsWith("foo-"))
                .forEach(e->log.info("foo pattern header: {}:{}", e.getKey(), e.getValue()));
        log.info("someCookie: {}", someCookie);

        return phrase==null
                ? stellarService.getSystems()
                : stellarService.getSystemsByName(phrase);
    }

    @GetMapping("/systems/{id}")
    ResponseEntity<PlanetarySystem> getSystemById(@PathVariable("id") int id){
        log.info("about to retrieve system {}", id);

        PlanetarySystem system = stellarService.getSystemById(id);
        if(system==null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(system);
        }
    }

    @GetMapping("/systems/{systemId}/planets")
    ResponseEntity<List<Planet>> getPlanetsBySystemId(@PathVariable("systemId") int systemId){
        log.info("about to retrieve planets of system {}", systemId);

        PlanetarySystem system = stellarService.getSystemById(systemId);
        if(system!=null){
            List<Planet> planets = stellarService.getPlanets(system);
            return ResponseEntity.ok(planets);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/systems")
    ResponseEntity<?> addSystem(
            @Validated @RequestBody PlanetarySystem planetarySystem,
            Errors errors,
            HttpServletRequest request
    ) {
        log.info("adding a new system...");

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
            List<String> messages = errors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            return ResponseEntity.badRequest().body(messages);
        }
        PlanetarySystem system = stellarService.addPlanetarySystem(planetarySystem);

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of(
                        "id", system.getId()
                ));
        return ResponseEntity
                .created(uri)
                .body(system);
    }



}
