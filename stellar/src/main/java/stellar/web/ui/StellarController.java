package stellar.web.ui;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;
import stellar.service.StellarService;

import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/ui")
public class StellarController {

    private final StellarService service;

    @GetMapping("/systems")
    String getSystems(Model model, @RequestParam(value = "phrase", required = false) String phrase) {
        log.info("about to present systems");

        List<PlanetarySystem> systems = phrase == null
                ? service.getSystems()
                : service.getSystemsByName(phrase);

        model.addAttribute("systems", systems);

        return "systems";
    }

    @GetMapping("/planets")
    String getPlanets(Model model, @RequestParam("systemId") int systemId){
        log.info("about to present planets");

        PlanetarySystem system = service.getSystemById(systemId);

        if (system == null) {
            model.addAttribute("error_message", "No system for id " + systemId);
            return "error";
        }

        List<Planet> planets = service.getPlanets(system);

        model.addAttribute("system", system);
        model.addAttribute("planets", planets);

        return "planets";
    }

    @ModelAttribute
    void addSlogan(Model model) {
        model.addAttribute(
                "slogan", "Sky is the limit :)");
    }

    @GetMapping("/addSystem")
    String addSystemPrepare(Model model){
        PlanetarySystem system = new PlanetarySystem();
        system.setDiscovery(new Date());
        system.setName("Brand new system");
        model.addAttribute("systemForm", system);
        return "addSystem";
    }

    @PostMapping("/addSystem")
    String addSystem(@Validated @ModelAttribute("systemForm") PlanetarySystem systemForm, Errors errors){

        if(errors.hasErrors()){
            return "addSystem";
        }

        systemForm = service.addPlanetarySystem(systemForm);

        return "redirect:/ui/systems";
    }

}
