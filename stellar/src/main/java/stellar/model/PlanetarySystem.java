package stellar.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.ToString;

import java.util.Date;
import java.util.List;


@Data
@ToString(exclude = "planets")
public class PlanetarySystem {

    private int id;
    @Size(min = 1)
    private String name;
    @NotNull
    private String star;
    private Date discovery;
    @Positive
    private float distance;
    @JsonIgnore
    private List<Planet> planets;

}
