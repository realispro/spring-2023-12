package stellar.model;

import lombok.Data;

@Data
public class Planet {

    private int id;
    private String name;
    private int size;
    private int weight;
    private int moons;
    private PlanetarySystem system;

}
