package stellar.dao.mem;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import stellar.dao.PlanetDAO;
import stellar.model.Planet;
import stellar.model.PlanetarySystem;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Slf4j
@Repository
public class InMemoryPlanetDAO implements PlanetDAO {


    @Override
    public List<Planet> getAllPlanets() {
        return InMemory.systems.stream().map(s->s.getPlanets()).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        log.info("fetching planets of {}", system );

        return InMemory.systems.stream().filter(s->s.equals(system)).findFirst().get().getPlanets();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {

        log.info("fetching planets of {} using phrase {}", system, like);
        return InMemory.systems.stream().filter(s->s.equals(system)).findFirst().get().getPlanets().stream().filter(p->p.getName().contains(like)).collect(Collectors.toList());
    }

    @Override
    public Planet getPlanetById(int id) {
        return getAllPlanets().stream().filter(p->p.getId()==id).findFirst().get();
    }

    @Override
    public Planet addPlanet(Planet p) {
        int idMax = getAllPlanets().stream().sorted((p1,p2)->p2.getId()-p1.getId()).findFirst().get().getId();
        p.setId(++idMax);
        InMemory.systems.stream().filter(s->s.getId()==p.getSystem().getId()).findFirst().get().getPlanets().add(p);
        return p;
    }
}
