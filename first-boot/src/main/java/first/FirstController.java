package first;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {

    @GetMapping("/bye")
    String sayGoodBye(){
        return "Merry Christmas!";
    }
}
