package spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import spring.config.Air;
import spring.config.ExecutionTime;
import spring.event.TravelFinishedEvent;
import spring.event.TravelStartedEvent;

@Component
public class Travel {

    private final String name;
    private final Transportation transportation;
    private final Accomodation accomodation;

    private final ApplicationEventPublisher publisher;

    @Autowired
    public Travel(
            @Air(fast = true) Transportation transportation,
            Accomodation accomodation,
            String name,
            ApplicationEventPublisher publisher) {
        this.transportation = transportation;
        this.accomodation = accomodation;
        this.name = name;
        this.publisher = publisher;
        System.out.println("constructing travel with parametrized constructor...");
    }

    @ExecutionTime
    public void travel(Person p){
        System.out.println("started travel " + name + " for a person " + p);
        publisher.publishEvent(new TravelStartedEvent(this, p));
        transportation.transport(p);
        accomodation.host(p);
        transportation.transport(p);
        publisher.publishEvent(new TravelFinishedEvent(this, p));
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Travel{" +
                "name='" + name + '\'' +
                ", transportation=" + transportation +
                ", accomodation=" + accomodation +
                '}';
    }
}
