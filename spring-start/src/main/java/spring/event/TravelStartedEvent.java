package spring.event;

import spring.Person;

public class TravelStartedEvent extends TravelEvent {

    public TravelStartedEvent(Object source, Person person) {
        super(source, person);
    }

}
