package spring.event;

import org.springframework.context.ApplicationEvent;
import spring.Person;

public abstract class TravelEvent extends ApplicationEvent {

    private final Person person;

    public TravelEvent(Object source, Person person) {
        super(source);
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
