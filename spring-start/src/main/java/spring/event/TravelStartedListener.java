package spring.event;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class TravelStartedListener implements ApplicationListener<TravelEvent> {
    @Override
    public void onApplicationEvent(TravelEvent event) {
        System.out.println("[TRAVEL] " + event.getClass().getName() + " for a person " + event.getPerson());
    }
}
