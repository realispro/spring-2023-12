package spring.config;

import org.springframework.context.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan("spring")
@PropertySource("classpath:/meals.properties")
@EnableAspectJAutoProxy
public class TravelConfig {

    @Bean
    String travelName(){
        return "Summer holiday 2024";
    }

    @Bean(name="meals")
    List<String> meals(){
        return new ArrayList<>();
    }

}
