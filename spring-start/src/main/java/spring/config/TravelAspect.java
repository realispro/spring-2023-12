package spring.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import spring.Person;
import spring.Ticket;
import spring.TravelException;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

@Component
@Aspect
public class TravelAspect {

    @Pointcut("execution(public * spring.impl..*(..))")
    void allInImpl(){}

    @Pointcut("execution(public * *(spring.Person))")
    void allAcceptingPerson(){}

    @Before("allInImpl()")
    void logEnteringMethod(JoinPoint jp){
        System.out.println("[AOP] entering method " + jp.getSignature().toLongString()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }

    @After("allInImpl()")
    void logExitingMethod(JoinPoint jp){
        System.out.println("[AOP] exiting method " + jp.getSignature().toLongString()
                + " on " + jp.getTarget().getClass().getSimpleName());
    }



    @Before("allAcceptingPerson()")
    void validateTicket(JoinPoint jp) {

        Person person = (Person)jp.getArgs()[0];
        Ticket ticket = person.getTicket();

        if (Objects.isNull(ticket) || ticketIsNotValid(ticket)) {
            System.out.println("[VALIDATE] ticket is empty or not valid");
            throw new RuntimeException("Travel interrupted because of invalid ticket");
        }
    }

    private boolean ticketIsNotValid(Ticket ticket) {
        System.out.println("[VALIDATE] ticket is valid unitl " + ticket.getValid());
        return ticket.getValid().isBefore(LocalDate.now());
    }

    @AfterThrowing(value = "allAcceptingPerson()", throwing = "e")
    void repackageToTravelException(RuntimeException e){
        throw new TravelException("repackaged", e);
    }

    @Around("@annotation(spring.config.ExecutionTime) || allInImpl()")
    Object measureExecutionTime(ProceedingJoinPoint jp) throws Throwable {

        Instant start = Instant.now();
        Object result = jp.proceed(jp.getArgs());
        Instant end = Instant.now();
        System.out.println("[AOP] " + jp.toLongString() + " execution time " + Duration.between(start, end));

        return result;
    }

}
