package spring.impl;

import org.springframework.beans.factory.FactoryBean;

import java.util.ArrayList;
import java.util.List;

public class Kitchen implements FactoryBean<List<String>> {
    @Override
    public List<String> getObject() throws Exception {
        return new ArrayList<>();
    }

    @Override
    public Class<?> getObjectType() {
        return List.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
