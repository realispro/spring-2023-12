package spring.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import spring.Accomodation;
import spring.Person;

import java.util.ArrayList;
import java.util.List;

@Component("accomodation")
public class Hotel implements Accomodation {

    private List<String> meals;

    @Override
    public void host(Person p) {
        System.out.println("person " + p + " is being hosted in hotel. meal: " + meals);
    }

    @Autowired
    public void prepareMenu(@Value("#{'${meals.menu}'.split('[\\s,]')}") List<String> meals, @Value("#{'${meals.gratis:soda}'.toUpperCase()}") String gratis) {
        //String gratis = env.getProperty("meals.gratis");
        this.meals = new ArrayList<>(meals);
        this.meals.add(gratis);
    }

    /*@PostConstruct
    public void abc() throws Exception {
        System.out.println("Hotel bean created. Menu: " + meals);
    }

    @PreDestroy
    public void xyz() throws Exception {
        System.out.println("Hotel bean destroyed");
    }*/
}
