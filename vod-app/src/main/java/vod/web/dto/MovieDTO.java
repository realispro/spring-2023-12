package vod.web.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import vod.model.Director;
import vod.model.Movie;
import vod.web.validation.Uri;

@Data
public class MovieDTO {

    private int id;
    @Size(min=2, max=256)
    private String title;
    @NotNull
    @Uri
    private String poster;
    private int directorId;

    public static MovieDTO fromData(Movie data){
        if(data==null) return null;
        MovieDTO dto = new MovieDTO();
        dto.setId(data.getId());
        dto.setTitle(data.getTitle());
        dto.setPoster(data.getPoster());
        dto.setDirectorId(data.getDirector().getId());
        return dto;
    }

    public Movie toData(){
        Movie data = new Movie();
        data.setId(this.id);
        data.setTitle(this.title);
        data.setPoster(this.poster);

        Director director = new Director();
        director.setId(this.directorId);
        data.setDirector(director);

        return data;
    }
}
