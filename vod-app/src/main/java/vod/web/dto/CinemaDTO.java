package vod.web.dto;

import lombok.Data;
import vod.model.Cinema;

@Data
public class CinemaDTO {

    private int id;
    private String name;
    private String logo;

    public static CinemaDTO fromData(Cinema data){
        if(data==null) return null;
        CinemaDTO dto = new CinemaDTO();
        dto.setId(data.getId());
        dto.setName(data.getName());
        dto.setLogo(data.getLogo());
        return dto;
    }

}
