package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import vod.config.Adviced;
import vod.web.validation.MovieValidator;

@ControllerAdvice(annotations = Adviced.class)
@Slf4j
@RequiredArgsConstructor
public class VodAdvice {

    private final MovieValidator movieValidator;

    @InitBinder("movieDTO")
    void initBinder(WebDataBinder binder){
        binder.addValidators(movieValidator);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e){
        log.error("illegal argument", e);
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.LOCKED)
    @ResponseBody
    String handleException(Exception e){
        log.error("exception", e);
        return e.getMessage();
    }
}
