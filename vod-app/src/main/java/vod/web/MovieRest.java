package vod.web;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import vod.config.Adviced;
import vod.model.Cinema;
import vod.model.Director;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.MovieDTO;

import java.net.URI;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

@RestController
@Slf4j
@RequiredArgsConstructor
@Adviced
public class MovieRest {

    private final MovieService movieService;
    private final CinemaService cinemaService;
    private final MessageSource messageSource;
    private final LocaleResolver localeResolver;


    @GetMapping("/movies")
    List<MovieDTO> getMovies(){
        log.info("about to retrieve movies");

        return movieService.getAllMovies().stream()
                .map(MovieDTO::fromData)
                .toList();
    }

    @GetMapping("/movies/{movieId}")
    ResponseEntity<MovieDTO> getMovie(@PathVariable("movieId") int movieId){
        log.info("about to retrieve movie {}", movieId);
        Movie movie = movieService.getMovieById(movieId);
        return ResponseEntity.of(Optional.ofNullable(MovieDTO.fromData(movie)));
    }

    @GetMapping("/directors/{directorId}/movies")
    ResponseEntity<List<MovieDTO>> getMoviesByDirector(@PathVariable("directorId") int directorId) {
        Director director = movieService.getDirectorById(directorId);

        if (director == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.of(Optional.ofNullable(movieService.getMoviesByDirector(director).stream().map(MovieDTO::fromData).toList()));
    }

    @GetMapping("/cinemas/{cinemaId}/movies")
    ResponseEntity<List<MovieDTO>> getMoviesInCinema(@PathVariable("cinemaId") int cinemaId){
        log.info("about to retrieve movies played in cinema {}", cinemaId);

        Cinema cinema = cinemaService.getCinemaById(cinemaId);

        if(cinema!=null) {
            return ResponseEntity.ok(cinemaService.getMoviesInCinema(cinema).stream()
                    .map(MovieDTO::fromData)
                    .toList());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/movies")
    ResponseEntity<?> addMovie(
            @Validated @RequestBody MovieDTO movieDTO,
            Errors errors,
            HttpServletRequest request
            ){
        log.info("about to add movie {}", movieDTO);

        if(errors.hasErrors()){
            Locale locale = localeResolver.resolveLocale(request);
                    //new Locale("pl", "PL");
            // messages_it_IT
            // messages_it
            // messages_<OS_LANG>
            // messages

            List<String> messages = errors.getAllErrors().stream()
                    .map(oe->messageSource.getMessage(oe.getCode(), oe.getArguments(), locale))
                    .toList();
            return ResponseEntity.badRequest().body(messages);
        }

        if(movieDTO.getTitle().equals("ups")){
            throw new IllegalStateException("ups");
        }

        Movie movie = movieService.addMovie(movieDTO.toData());

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(Map.of(
                        "id", movie.getId()
                ));

        return ResponseEntity
                .created(uri)
                .body(MovieDTO.fromData(movie));

    }




}
