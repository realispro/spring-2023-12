package vod.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import vod.model.Cinema;
import vod.model.Movie;
import vod.service.CinemaService;
import vod.service.MovieService;
import vod.web.dto.CinemaDTO;

import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CinemaRest {

    private final CinemaService cinemaService;
    private final MovieService movieService;

    @GetMapping("/cinemas")
    List<CinemaDTO> getCinemas() {
        log.info("retrieving cinemas");

        return cinemaService.getAllCinemas().stream()
                .map(CinemaDTO::fromData)
                .toList();
    }

    @GetMapping("/cinemas/{cinemaId}")
    ResponseEntity<CinemaDTO> getCinemaById(@PathVariable("cinemaId") int cinemaId) {
        log.info("retrieving cinema by id {}", cinemaId);

        Cinema cinema = cinemaService.getCinemaById(cinemaId);
        return ResponseEntity.of(Optional.ofNullable(CinemaDTO.fromData(cinema)));
    }

    @GetMapping("/movies/{movieId}/cinemas")
    ResponseEntity<List<CinemaDTO>> getCinemasByMovieId(@PathVariable(value = "movieId") int movieId) {
        log.info("retrieving cinemas that play movie id {}", movieId);
        Movie movie = movieService.getMovieById(movieId);

        if (movie == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(
                cinemaService.getCinemasByMovie(movie).stream()
                        .map(CinemaDTO::fromData)
                        .toList()
        );
    }
}